const express = require('express')
const fs = require('fs').promises;
const app = express()
const port = 3000

const mensajes = [
    "hola",
    "q tal",
    "q onda",
    "buena"
]

let msgIdx = 0;

app.use(express.json())

app.use( (req, res, next) => {
    console.log("new request", req.path);
    console.log('Time: %d', Date.now());
    next();
})

app.use( (req, res, next) => {
    if (req.headers['token'] !== 'cuac') {
        res.set('barsa', 'true');
        res.status(401).send('NO Autorizado');
        return;
    }
    next()
})

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/nuni', (req, res) => {
    res.send({ text: 'nuni, perro' })
})

app.get('/nuni/:filename', async (req, res) => {
    const filename = req.params.filename;
    console.log("requested filename", filename)
    try {
        const content = await fs.readFile('/tmp/' + filename);
        res.send({ text: content.toString() })
    } catch(e) {
        console.log(e)
        res.status(305).send({text: "file does not exist"});
        return;
    }
})

app.post('/destruct', (req, res) => {
    const { n1, n2, n3, n4, arr } = req.body;
    const [,,x3,] = arr;

    console.log(x3);

    res.send({texto: n1+n2+n3+n4})
});

app.post('/nuni/:filename', async (req, res) => {
    const filename = req.params.filename;
    console.log("requested filename", filename)
    console.log(req.body)
    try {
        const data = new Uint8Array(Buffer.from(JSON.stringify(req.body)));
        const content = await fs.writeFile('/tmp/' + filename, data);
        res.send({ text: content })
    } catch(e) {
        console.log(e)
        res.status(305).send({text: "There was an error"});
        return;
    }
})

app.get('/log/:filename', async (req, res) => {
    const filename = req.params.filename;
    console.log("requested filename", filename)
    try {
        const content = await fs.readFile('/tmp/' + filename);
        const logEntry = {
            date: Date.now(),
            text: content.toString()
        }
        const data = new Uint8Array(Buffer.from(JSON.stringify(logEntry)));
        await fs.appendFile('/tmp/log.txt', data);
        const log = await fs.readFile('/tmp/log.txt');
        res.send({"text": log.toString()});
    } catch (e) {
        console.log(e);
        res.status(305).send({text: "hubo un error"});
    }
})

app.get('/info', async (req, res) => {
    try {
        const content = await fs.readFile('/tmp/info.txt');
        res.send({ text: content.toString() })
    } catch(e) {
        console.log(e)
        res.status(305).send({text: "file does not exist"});
        return;
    }
})

app.get('/saludo', (req, res) => {
    res.send({"texto": mensajes[msgIdx]});
});

setInterval( () => {
    console.log("changing message")
    msgIdx = Math.floor(Math.random() * mensajes.length)
}, 1000);


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})